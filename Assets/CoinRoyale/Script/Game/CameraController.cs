using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace RivalArts.CoinRoyale
{

    public class CameraController : MonoBehaviour
    {
        public Transform observedTransform;


        void LateUpdate()
        {
            
            if(observedTransform != null)
            {
                Vector3 new_pos= observedTransform.position;
                new_pos.y= this.transform.position.y;
                this.transform.position= new_pos;
            }
        }
    }
}
