using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon;
using Photon.Pun;

namespace RivalArts.CoinRoyale
{
    public class CharAnimController : MonoBehaviour
    {

        #if UNITY_EDITOR 
        [Header("REFERENCES")]
        #endif

        [SerializeField]
        private PhotonView photonView;


        [SerializeField]
        private Animator animator;

        [SerializeField]
        private new Rigidbody rigidbody;


        [SerializeField]
        private CharJumpController charJumpController;        

        // Start is called before the first frame update
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {
            if(photonView.IsMine)
            {
            
                if  (
                        ( Mathf.Abs(rigidbody.velocity.x) > 0.01f) ||
                        ( Mathf.Abs(rigidbody.velocity.z) > 0.01f)
                    )
                {
                    animator.SetBool("walking", true);
                }else
                {
                    animator.SetBool("walking", false);
                }

                animator.SetBool("jumping", charJumpController.isJumping);
            }
        
        }
    }

}