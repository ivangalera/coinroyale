using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon;
using Photon.Pun;

namespace RivalArts.CoinRoyale
{
    public class CharJumpController : MonoBehaviour
    {
        #if UNITY_EDITOR 
        [Header("PARAMS")]
        #endif

        public float startJumpVelocity= 6.0f;

        public float yVelocityLandingThreshold= 0.05f;
        
        #if UNITY_EDITOR 
        [Header("REFERENCES")]
        #endif


        [SerializeField]
        private PlayerInputController playerInputController;

        [SerializeField]
        private new Rigidbody rigidbody;

        [SerializeField]
        private PhotonView photonView;

        
        #if UNITY_EDITOR 
        [Header("BEHAVIOUR")]
        #endif

        [SerializeField]
        public bool isJumping= false;
        public float lastJumpVelocity;

        // Update is called once per frame
        private void Update()
        {
            HandleJump();
        }

        private void FixedUpdate()
        {
            StartJump();
            
        }

        

        private void StartJump()
        {
            if(photonView.IsMine)
            {
                if(playerInputController.jumpInput)
                {
                    if(!isJumping)
                    {
                        this.rigidbody.velocity= new Vector3(this.rigidbody.velocity.x, startJumpVelocity, this.rigidbody.velocity.z);
                        lastJumpVelocity= startJumpVelocity;
                        isJumping= true;
                    }
                }


                //Vector3 new_rgbdy_velocity= new Vector3(this.rigidbody.x, this.rigidbody.velocity.y, this.rigidbody.z);
                        
                //this.rigidbody.velocity= new_rgbdy_velocity;
            }
        }

        private void HandleJump()
        {
            if(isJumping)
            {
                if(rigidbody.velocity.y > lastJumpVelocity)
                {
                    isJumping= false;
                }
                
            }
            lastJumpVelocity= this.rigidbody.velocity.y;
        }
        
    }
}