using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using Photon.Pun;

namespace RivalArts.CoinRoyale
{
    public class CharUINameController : MonoBehaviour
    {

        #if UNITY_EDITOR 
        [Header("REFERENCES")]
        #endif

        [SerializeField]
        private PhotonView photonView;

        public Text nameLabel;



        // Start is called before the first frame update
        void Start()
        {
            if(!photonView.IsMine)
            {

                nameLabel.transform.SetParent(LevelManager.Instance.canvas.transform);

                nameLabel.text= photonView.Owner.NickName;
            }else
            {
                nameLabel.gameObject.SetActive(false);
            }
            
        }

        
    }

}
