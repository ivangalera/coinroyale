using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon;
using Photon.Pun;

namespace RivalArts.CoinRoyale
{
    public class CharWalkController : MonoBehaviour
    {
        #if UNITY_EDITOR 
        [Header("PARAMS")]
        #endif

        public float walkVelocity= 6.0f;

        
        #if UNITY_EDITOR 
        [Header("REFERENCES")]
        #endif


        [SerializeField]
        private PlayerInputController playerInputController;

        [SerializeField]
        private CharJumpController charJumpController;

        [SerializeField]
        private new Rigidbody rigidbody;

        [SerializeField]
        private PhotonView photonView;

        


        // Update is called once per frame
        private void Update()
        {
            
            UpdateFacingDir();
        }

        private void FixedUpdate()
        {
            UpdateRigidbodyVelocity();
            
        }

        

        private void UpdateRigidbodyVelocity()
        {
            if(photonView.IsMine)
            {
                if(!charJumpController.isJumping)
                {
                    Vector3 plane_velocity= Vector3.ClampMagnitude(walkVelocity*playerInputController.walkInput, walkVelocity);

                    Vector3 new_rgbdy_velocity= new Vector3(plane_velocity.x, this.rigidbody.velocity.y, plane_velocity.z);
                            
                    this.rigidbody.velocity= new_rgbdy_velocity;
                }
            }
        }

        private void UpdateFacingDir()
        {
            if(photonView.IsMine)
            {
                if  ( 
                        (Mathf.Abs(Input.GetAxis("Horizontal") ) > 0.2f)  ||
                        (Mathf.Abs(Input.GetAxis("Vertical") ) > 0.2f)
                    )
                {
                    this.transform.rotation= Quaternion.LookRotation(playerInputController.walkInput.normalized, Vector3.up );
                }
            }
        }
        
    }
}