using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon;
using Photon.Pun;

namespace RivalArts.CoinRoyale
{
    public class CoinController : MonoBehaviour
    {
        #if UNITY_EDITOR 
        [Header("PARAMETERS")]
        #endif
        public float respawnTime= 5.0f;


        #if UNITY_EDITOR 
        [Header("REFERENCES")]
        #endif

        [SerializeField]
        private PhotonView photonView;

        [SerializeField]
        private Renderer coinMeshRenderer;

        [SerializeField]
        private Light coinLight;

        [SerializeField]
        private ParticleSystem spawnParticleSystem;

        [SerializeField]
        private ParticleSystem pickParticleSystem;

        public enum CoinStatusEnum
        {
            HIDDEN= 0,
            PICKABLE= 1
        }

        #if UNITY_EDITOR 
        [Header("BEHAVIOUR")]
        #endif


        [SerializeField]      
        private CoinStatusEnum coinStatus= CoinStatusEnum.HIDDEN;

        [SerializeField]      
        float currentRespawnTime;

        [SerializeField]
        bool waitingForMasterClientSync;

        // Start is called before the first frame update
        void Start()
        {
            if(PhotonNetwork.IsMasterClient)
            {
                this.transform.position= LevelManager.Instance.CoinSpawnPoint[GetRandomSpawnPointIndex()].position;
            }
            pickParticleSystem.transform.SetParent(pickParticleSystem.transform.parent.parent);
        }

        // Update is called once per frame
        void Update()
        {
             if(coinStatus == CoinController.CoinStatusEnum.HIDDEN)
             {
                currentRespawnTime-= Time.deltaTime;
                if(currentRespawnTime <= 0)
                {
                    if(PhotonNetwork.IsMasterClient)
                    {
                        this.photonView.RPC("RPCSpawn", RpcTarget.All );
                    }
                }
             }
        }

        public void OnTriggerEnter(Collider other)
        {
            #if UNITY_EDITOR
            Debug.Log("Coin detects trigger");
            #endif

            if(other.gameObject.tag == GameManager.TAG_PLAYER )
            {
                PhotonView playerPhotonView= other.GetComponent<PhotonView>();
                if(playerPhotonView.IsMine)
                {
                    playerPhotonView.RPC("RPCTryToPickCoin",RpcTarget.MasterClient);
                }
            }
            
        }

        public void TryToPick(PlayerCoinPickerController player)
        {
            #if UNITY_EDITOR
            Debug.Log("_coinStatus: " + coinStatus);
            #endif

            if(coinStatus == CoinController.CoinStatusEnum.PICKABLE)
            {
                player.GetComponent<PhotonView>().RPC("RPCPickCoin", RpcTarget.All, GetRandomSpawnPointIndex() );
            }
        }

        public void DoPick(int next_coin_spawn_point_index)
        {
            #if UNITY_EDITOR
            Debug.Log("Pick Particle Play");
            #endif

            pickParticleSystem.time= 0;
            pickParticleSystem.Play();


            coinStatus= CoinController.CoinStatusEnum.HIDDEN;
            coinMeshRenderer.enabled= false;
            coinLight.enabled= false;

            this.transform.position= LevelManager.Instance.CoinSpawnPoint[next_coin_spawn_point_index].position;
            currentRespawnTime= respawnTime;

        }

        public void Spawn()
        {
            coinStatus= CoinController.CoinStatusEnum.PICKABLE;
            coinMeshRenderer.enabled= true;
            coinLight.enabled= true;
            
            spawnParticleSystem.time= 0;
            spawnParticleSystem.Play();

            pickParticleSystem.transform.position= this.transform.position;

        }

        private int GetRandomSpawnPointIndex()
        {
            return Random.Range(0, LevelManager.Instance.CoinSpawnPoint.Length);
        }

        public void SyncFromMasterClient()
        {
            #if UNITY_EDITOR
            Debug.Log("REQUEST SYNC");
            #endif

            waitingForMasterClientSync= true;

            this.photonView.RPC("RPCRequestCoinSync", RpcTarget.MasterClient);
        }

        [PunRPC]
        public void RPCSpawn()
        {
            this.Spawn();
        }

        [PunRPC]
        public void RPCRequestCoinSync()
        {

            this.photonView.RPC("RPCSyncCoinBehaviour", RpcTarget.All, (int)coinStatus, currentRespawnTime);
        }
        

        [PunRPC]
        public void RPCSyncCoinBehaviour(int coin_status, float current_respawn_time)
        {
            if(waitingForMasterClientSync)
            {
                #if UNITY_EDITOR
                Debug.Log("GET SYNC");
                #endif
                
                this.coinStatus= (CoinStatusEnum) coin_status;
                this.currentRespawnTime= current_respawn_time;

                coinMeshRenderer.enabled= (coinStatus == CoinStatusEnum.PICKABLE);
                waitingForMasterClientSync= false;
            }
        }

    }
}
