using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

using Photon;
using Photon.Pun;

using Hashtable= ExitGames.Client.Photon.Hashtable;


namespace RivalArts.CoinRoyale
{
    public class GameManager : MonoBehaviour
    {

        public static string TAG_PLAYER= "TagPlayer";
        public static string TAG_COIN= "Coin";
        
        public const string HASHKEY_PLAYER_READY = "IsPlayerReady";
        public const string HASHKEY_PLAYER_LOADED_LEVEL = "PlayerLoadedLevel";

        public const string HASHKEY_RANDOMIZE_PLAYER_SPAWN = "RandomizeSpawn";
        public static GameManager Instance;


        private void Awake()
        {
            if(GameManager.Instance != null)
            {
                #if UNITY_EDITOR
                Debug.Log("Destroy old Game Manager");
                #endif
                Destroy(GameManager.Instance);
            }
            
            GameManager.Instance= this;

            DontDestroyOnLoad(this.gameObject);
            
        }

        public void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            SceneManager.sceneLoaded -= RivalArts.CoinRoyale.GameManager.Instance.OnSceneLoaded;

            #if UNITY_EDITOR
            Debug.Log("ESCENA CARGADA");
            #endif

            SpawnPlayer();
            StartCoroutine( SyncCoin() );
        }



        private void SpawnPlayer()
        {
            
            
            Vector3 spawn_pos= LevelManager.Instance.GetPlayerSpawnPosition() +  new Vector3(0, 2.5f, 0);
            GameObject plyrGO= PhotonNetwork.Instantiate("prfPlayer", spawn_pos, Quaternion.identity, 0);

            LevelManager.Instance.playerCamera.observedTransform= plyrGO.transform;
        }

        IEnumerator SyncCoin()
        {
            if(!PhotonNetwork.IsMasterClient)
            {
                while(!LevelManager.Instance)
                {
                    //Espera hasta que LevelManager se instancie

                    yield return null;
                }

                LevelManager.Instance.coin.SyncFromMasterClient();
            }
        }
    }

    
}
