using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

using Photon;
using Photon.Pun;

namespace RivalArts.CoinRoyale
{
    public class LevelManager : MonoBehaviour
    {
        
        public static LevelManager Instance;

        #if UNITY_EDITOR 
        [Header("REFERENCES")]
        #endif

        public Canvas canvas;
        
        public CoinController coin;

        public CameraController playerCamera;

        public UIPlayerInfoList uIPlayerInfoList;

        public Transform[] CoinSpawnPoint;

        public Transform[] playerSpawnPoint;

        #if UNITY_EDITOR 
        [Header("BEHAVIOUR")]
        #endif

        //[SerializeField]
        //private bool firstSpawnDone= false;

        [SerializeField]
        private int playerSpawnIndex;

        private void Awake()
        {
            if(LevelManager.Instance != null)
            {
                #if UNITY_EDITOR
                Debug.Log("Destroy old LevelManager");
                #endif
                Destroy(LevelManager.Instance);
            }
            
            LevelManager.Instance= this;

            DontDestroyOnLoad(this.gameObject);
            
        }

        
        public Vector3 GetPlayerSpawnPosition()
        {
            /*if(!firstSpawnDone)
            {
                playerSpawnIndex= Random.Range(0, playerSpawnPoint.Length );
                firstSpawnDone= true;
            }*/

            playerSpawnIndex= (int)PhotonNetwork.CurrentRoom.CustomProperties[GameManager.HASHKEY_RANDOMIZE_PLAYER_SPAWN];

            int p= 0;
            while   ( 
                        ( p < PhotonNetwork.PlayerList.Length) && 
                        ( PhotonNetwork.PlayerList[p].UserId != PhotonNetwork.LocalPlayer.UserId ) 
                    )
            {
                p++;
            }

            playerSpawnIndex= (playerSpawnIndex + p) % playerSpawnPoint.Length;

            return playerSpawnPoint[playerSpawnIndex].position;
        }
    }
}
