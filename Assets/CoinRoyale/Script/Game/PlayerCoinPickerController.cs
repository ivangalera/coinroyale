using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon;
using Photon.Pun;

namespace RivalArts.CoinRoyale
{

    public class PlayerCoinPickerController : MonoBehaviour
    {

        #if UNITY_EDITOR 
        [Header("REFERENCES")]
        #endif

        [SerializeField]
        private PhotonView photonView;

        
        [SerializeField]
        private PlayerScoreController playerScoreController;

      

        private void PickCoin(int next_coin_spawn_point_index)
        {
            #if UNITY_EDITOR
            Debug.Log("Player Picked Coin Confirmed");
            #endif
            
            LevelManager.Instance.coin.DoPick(next_coin_spawn_point_index);
            
            playerScoreController.IncScore();
        }


        [PunRPC]
        public void RPCTryToPickCoin()
        {

            //if(PhotonNetwork.IsMasterClient) //Innecesario, porque este RPC siempre se envía al masterclient
            {
                #if UNITY_EDITOR
                Debug.Log("Player in MasterClient TryToPick");
                #endif
                LevelManager.Instance.coin.TryToPick(this);
            }
        }

        [PunRPC]
        public void RPCPickCoin(int next_coin_spawn_point_index)
        {
            PickCoin(next_coin_spawn_point_index);
        }
        
    }

}