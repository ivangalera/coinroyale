using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon;
using Photon.Pun;

namespace RivalArts.CoinRoyale
{
    public class PlayerInputController : MonoBehaviour
    {
        #if UNITY_EDITOR 
        [Header("REFERENCES")]
        #endif

        [SerializeField]
        private PhotonView photonView;


        #if UNITY_EDITOR 
        [Header("BEHAVIOUR")]
        #endif

        public Vector3 walkInput= Vector3.zero;
        public bool jumpInput;

        // Update is called once per frame
        void Update()
        {
            HandleInput();
        }

        private void HandleInput()
        {
            if(photonView.IsMine)
            {
                walkInput.Set( Input.GetAxis("Horizontal"), 0 , Input.GetAxis("Vertical") );
                jumpInput= Input.GetButton("Jump");

            }
        }

        

    }
}
