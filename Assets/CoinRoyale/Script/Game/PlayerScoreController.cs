using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon;
using Photon.Pun;
using Hashtable= ExitGames.Client.Photon.Hashtable;

using Photon.Pun.UtilityScripts;

namespace RivalArts.CoinRoyale
{

    public class PlayerScoreController : MonoBehaviour
    {

        public static string CUSTOM_PROP_SCORE= "score";

        #if UNITY_EDITOR 
        [Header("REFERENCES")]
        #endif

        [SerializeField]
        private PhotonView photonView;

        #if UNITY_EDITOR 
        [Header("BEHAVIOUR")]
        #endif

        public int score; 


        public void IncScore()
        {
            if(photonView.IsMine)
            {
                PhotonNetwork.LocalPlayer.AddScore(1);

                score= PhotonNetwork.LocalPlayer.GetScore();
            }
        }

        private void Awake()
        {
            if(photonView.IsMine)
            {
                score= 0;
                PhotonNetwork.LocalPlayer.SetScore(score);

            }
        }

        /*
        private Hashtable customProperties = new Hashtable();


        private void Awake()
        {
            if(photonView.IsMine)
            {
                customProperties.Add(PlayerScoreController.CUSTOM_PROP_SCORE,  score);

                bool r= PhotonNetwork.LocalPlayer.SetCustomProperties(this.customProperties);

                #if UNITY_EDITOR
                Debug.Log("Custom prop SET ("+r+") " + PhotonNetwork.LocalPlayer.UserId);
                #endif
            }
        }

        public void IncScore()
        {
            score++;
            if(photonView.IsMine)
            {
                customProperties[PlayerScoreController.CUSTOM_PROP_SCORE]= score;

                PhotonNetwork.LocalPlayer.SetCustomProperties(this.customProperties);

                //LevelManager.Instance.uIPlayerInfoList.UpdatePlayersList();
            }
        }*/


        
    }

}