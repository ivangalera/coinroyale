using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class UIPlayerInfo : MonoBehaviour
{
    #if UNITY_EDITOR 
    [Header("REFERENCES")]
    #endif

    public Text playerNameText;
    public Text playerScoreText;


}
