using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using Photon;
using Photon.Pun;

using Photon.Realtime;
using Hashtable= ExitGames.Client.Photon.Hashtable;

using Photon.Pun.UtilityScripts;


namespace RivalArts.CoinRoyale
{
    public class UIPlayerInfoList : MonoBehaviourPunCallbacks
    {

        #if UNITY_EDITOR 
        [Header("REFERENCES")]
        #endif

        public UIPlayerInfo[] uIPlayerInfo;


        // Start is called before the first frame update
        void Start()
        {
            //UpdatePlayersList();
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps)
        {
            UpdatePlayersList();
        }

        public void UpdatePlayersList()
        {
            Player[] players_list= new Player[uIPlayerInfo.Length];
            int registered_players= 0;
            bool local_player_registered= false;

            //foreach (Player player in PhotonNetwork.PlayerList) 
            for(int p= 0; p < PhotonNetwork.PlayerList.Length; p++)
            {
                #if UNITY_EDITOR
                //Debug.Log("checking player: "+p+" / "+PhotonNetwork.PlayerList.Length);
                //Debug.Log("ID: "+PhotonNetwork.PlayerList[p].UserId);
                Debug.Log("PN.nick: "+PhotonNetwork.PlayerList[p].NickName);
                //Debug.Log("CP: "+ (PhotonNetwork.PlayerList[p].CustomProperties != null) );
                //Debug.Log("PN.score: "+ (int)(PhotonNetwork.PlayerList[p].CustomProperties[PlayerScoreController.CUSTOM_PROP_SCORE] ) );
                Debug.Log("PN.score: "+ (int)(PhotonNetwork.PlayerList[p].GetScore() ) );
                #endif

                if(registered_players == 0)
                {

                    
                    players_list[0]= PhotonNetwork.PlayerList[p]; 
                    registered_players++;


                    if(players_list[0].IsLocal)
                    {
                        local_player_registered= true;
                    }

                }else
                {

                    Player testing_player= PhotonNetwork.PlayerList[p];

                    if(registered_players < players_list.Length) //ubica testing_player al final (si hay espadio) 
                    {
                        players_list[registered_players]= testing_player;

                        if(players_list[registered_players].IsLocal)
                        {
                            local_player_registered= true;
                        }
                    }

                    int order_i= registered_players -1; //Se ubica indice en el registrado en el ultimo ciclo

                    //Se recorre la players_list desde el final hasta el inicio. Se continúa si el
                    //testing_player tiene score superior
                    while   (
                                (order_i >= 0)   && 
                                ( (int)testing_player.GetScore() >
                                                (int)players_list[order_i].GetScore() 
                                )
                            )
                    {
                        
                        //=> El score de testing_player es mayor que el de players_list[order_i]

                        //Se corre el players_list[order_i] un espacio atrás (si hay espacio)
                        if(order_i + 1 < players_list.Length) 
                        {
                            players_list[order_i + 1]= players_list[order_i];
                        }else
                        {
                            // => No hay espacio para correr hacia atras

                            if(players_list[order_i].IsLocal)
                            {
                                //Si es localPlayer -> indicar que testing_player lo sobreescribirá
                                local_player_registered= false;
                            }
                        }

                        //Se asigna el testing_player a la posición order_i
                        players_list[order_i]= testing_player;

                        if(players_list[order_i].IsLocal)
                        {
                            local_player_registered= true;
                        }

                        order_i--;
                    }

                    registered_players= Mathf.Min(registered_players + 1, players_list.Length );         
                }
            }

            if(!local_player_registered)
            {
                players_list[registered_players - 1]= PhotonNetwork.LocalPlayer;
            }

            for(int i= 0; i < uIPlayerInfo.Length; i++)
            {
                if( i < registered_players )
                {
                    #if UNITY_EDITOR
                    Debug.Log("pl["+i+"].nick: "+players_list[i].NickName);
                    Debug.Log("pl["+i+"].score: "+ ( players_list[i].GetScore().ToString() ) );
                    #endif       

                    uIPlayerInfo[i].playerNameText.text= players_list[i].NickName;
                    uIPlayerInfo[i].playerScoreText.text= (players_list[i].GetScore().ToString() );
                    uIPlayerInfo[i].gameObject.SetActive(true);
                }else
                {
                    uIPlayerInfo[i].gameObject.SetActive(false);
                }
            }
        }


    }
}
