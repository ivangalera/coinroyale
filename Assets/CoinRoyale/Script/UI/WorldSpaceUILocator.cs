using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace RivalArts.CoinRoyale
{
    public class WorldSpaceUILocator : MonoBehaviour
    {
        public Transform target;

        // Update is called once per frame
        void LateUpdate()
        {
            if(target != null)
            {
                this.transform.position= Camera.main.WorldToScreenPoint( target.position);
                //this.transform.rotation= LevelManager.Instance.playerCamera.transform.rotation;
                //this.transform.LookAt(Camera.main.transform.position);
            }
        }
    }
}
